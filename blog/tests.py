from django.test import TestCase
import datetime as dt
from blog.models import Day
from .views import predykcja


# Create your tests here.


class PredykcjaTest(TestCase):

    def is_predict_return_value(self):
        datetime = dt.date.today()
        predicted = predykcja(datetime)

        self.assertGreater(predicted, 0)
