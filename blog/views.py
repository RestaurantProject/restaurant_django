import pandas as pd
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from sklearn.linear_model import LinearRegression
from .models import Day
from .forms import DateForm


# Create your views here.

def home(request):
    return render(request, 'blog/home.html')


def predykcja(request):
    form = DateForm()
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            dateFromForm = form.cleaned_data['date']
            predicted = predict(dateFromForm)
            predictedText = getObciazenie(predicted)
            return render(request, 'blog/predykcja.html',
                          {'form': form, 'predicted': predicted, 'predictedText': predictedText})

    return render(request, 'blog/predykcja.html', {'form': form})



def predict(dateFromForm):
    data = pd.read_csv('seasonNumber.csv', sep=';')

    df = pd.DataFrame(data)

    df = df[['dayNumber', 'seasonNumber', 'customers']]

    X_t = df[['dayNumber', 'seasonNumber']]
    y_t = df['customers']

    pred_model = LinearRegression(normalize=True)

    pred_model.fit(X_t, y_t)

    predDay = getDayOfWeek(dateFromForm)
    predSeason = getSeason(dateFromForm)

    Xnew = [[predDay, predSeason]]

    predicted = pred_model.predict(Xnew)

    return predicted


def getObciazenie(liczba):
    if (liczba > 102):
        obciazenie = "bardzo duze"
    elif (liczba > 92):
        obciazenie = "duze"
    elif (liczba > 78):
        obciazenie = "srednie"
    elif (liczba > 66):
        obciazenie = "male"
    else:
        obciazenie = "bardzo male"

    return obciazenie


def getDayOfWeek(dateFromForm):
    return dateFromForm.weekday() + 1


def getSeason(date):
    # get the current day of the year
    doy = date.timetuple().tm_yday
    # "day of year" ranges for the northern hemisphere
    spring = range(80, 172)
    summer = range(172, 264)
    fall = range(264, 355)
    # winter = everything else

    if doy in spring:
        season = 1
    elif doy in summer:
        season = 2
    elif doy in fall:
        season = 3
    else:
        season = 4

    return season
