from django.db import models
from django.contrib.auth.models import User


class Day(models.Model):
    day = models.DateTimeField()
    dayNumber = models.IntegerField(default=0)
    seasonNumber = models.IntegerField(default=0)
    customers = models.IntegerField(default=0)
